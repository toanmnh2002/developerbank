﻿using DeveloperBank.Entities;
using DeveloperBank.Entitites;
using DeveloperBank.Interfaces;
using Newtonsoft.Json;
using System.Globalization;

namespace DeveloperBank.Services
{
    public class Logic : ILogic
    {
        public Queue<Customer> vipCustomers;
        public Queue<Customer> basicCustomers;
        public List<Customer> historyCustomers;
        //E:\Study\Backend\laptrinh\csharp\DeveloperBank\Data\BasicCustomers.json
        public Logic()
        {
            vipCustomers = new Queue<Customer>();
            basicCustomers = new Queue<Customer>();
            historyCustomers = new List<Customer>();
            LoadCustomersFromFile(@"Data\BasicCustomers.json", @"Data\VipCustomers.json");
        }

        #region Load Data
        private void LoadCustomersFromFile(string fileBasicPath = null, string fileVipPath = null)
        {
            if (fileBasicPath is not null)
            {
                if (File.Exists(fileBasicPath))
                {
                    string jsonContent = File.ReadAllText(fileBasicPath);
                    var basicCustomer = JsonConvert.DeserializeObject<Queue<Customer>>(jsonContent);

                    if (basicCustomer is null)
                    {
                        basicCustomer = new Queue<Customer>();
                    }
                    foreach (var customer in basicCustomer)
                    {
                        basicCustomers.Enqueue(customer);
                    }
                }
            }
            if (fileVipPath is not null)
            {
                if (File.Exists(fileVipPath))
                {
                    string jsonContent = File.ReadAllText(fileVipPath);
                    var vipCustomer = JsonConvert.DeserializeObject<Queue<Customer>>(jsonContent);
                    if (vipCustomer == null)
                    {
                        vipCustomer = new Queue<Customer>();
                    }
                    foreach (var item in vipCustomer)
                    {
                        vipCustomers.Enqueue(item);
                    }
                }
            }
        }
        #endregion

        #region Save Data
        public void SaveToFile()
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            currentDirectory = HandleString(currentDirectory);
            var filePath = Path.Combine(currentDirectory, "Data", "BasicCustomers.json");
            File.WriteAllText(filePath, JsonConvert.SerializeObject(basicCustomers.ToList()));
        }

        public string HandleString(string originalPath) => originalPath.IndexOf(@"\bin\Debug\net8.0") >= 0 ? originalPath.Substring(0, originalPath.IndexOf(@"\bin\Debug\net8.0")).Trim() : originalPath;
        #endregion


        public void AddCustomerToTheQueue()
        {
            Console.Write("Tên: ");
            var name = Console.ReadLine();

            Console.Write("Số tiền cần rút(vnđ): ");
            _ = decimal.TryParse(Console.ReadLine(), out decimal amount);

            var newCustomer = new Customer
            {
                Amount = amount,
                Name = name
            };

            if (amount > 500_000_000)
            {
                HandleCustomer(name, newCustomer, vipCustomers);
            }
            else
            {
                HandleCustomer(name, newCustomer, basicCustomers);
                SaveToFile();
            }
        }

        private void HandleCustomer(string name, Customer newCustomer, Queue<Customer> queue)
        {
            newCustomer.CustomerId = $"TH00{queue.Count + 1}";
            queue.Enqueue(newCustomer);
            Console.WriteLine($"\nKhách hàng {name} đã được thêm vào hàng đợi thường thành công thành công" +
              $"\n\nSố thứ tự:{newCustomer.CustomerId}");
            PrintEachCustomer(queue);
        }

        private void PrintHistoryCustomer()
        {
            int index = 0;
            _ = historyCustomers.ToList().Count is 0 ? throw new Exception("Danh sách rỗng!") : historyCustomers.Count;

            historyCustomers.ToList().ForEach(customer => Console.WriteLine($"Danh sách khách hàng đã rút tiền" +
                $"\n{index += 1}. Tên:{customer.Name}"));
        }

        private void PrintEachCustomer(Queue<Customer> queue)
        {
            var listCount = queue.Count is 0 ? throw new Exception("Danh sách rỗng!") : historyCustomers.Count;

            queue.ToList().ForEach(customer => Console.WriteLine($"\nSố thứ tự:{customer.CustomerId}" +
              $"\nTên:{customer.Name}" +
              $"\nSố tiền cần rút:{customer.Amount.ToString("C", new CultureInfo("vi-VN"))}"));
        }

        public void CallNextBasicCustomer()
        {
            _ = basicCustomers.Count is 0 ? throw new Exception($"Hàng đợi khách hàng thường trống.") : basicCustomers.Count;

            var customer = basicCustomers.Dequeue();
            historyCustomers.Add(customer);
            Console.WriteLine($"\n\nSố thứ tự: {customer.CustomerId}" +
               $"\nTên: {customer.Name}" +
               $"\n\nKhách hàng {customer.Name} đã rút tiền thành công.");
        }

        public void CallGetReadyNextCustomer(Queue<Customer> queue)
        {
            int index = 0;
            var customerCount = queue.Count is 0 ? throw new Exception($"Hàng đợi khách hàng trống.") : queue.Count;
            queue.Take(3).ToList().ForEach(customer => Console.WriteLine($"\n{index += 1}. {customer.CustomerId} - {customer.Name}"));
        }

        public void CallUpcomingCustomer()
        {
            Console.Write("\nHàng đợi VIP");
            CallGetReadyNextCustomer(vipCustomers);
            Console.Write("\nHàng đợi Thường");
            CallGetReadyNextCustomer(basicCustomers);
        }

        public void CallNextVipCustomer()
        {
            _ = vipCustomers.Count is 0 ? throw new Exception($"Hàng đợi khách hàng Vip trống.") : vipCustomers.Count;

            var customer = vipCustomers.Dequeue();

            historyCustomers.Add(customer);
            Console.WriteLine($"\n\nSố thứ tự: {customer.CustomerId}" +
               $"\nTên: {customer.Name}" +
               $"\n\nKhách hàng {customer.Name} đã rút tiền thành công.");
        }

        public ReportBank ReportSystem()
        {
            int index = 0;
            var reportBank = new ReportBank
            {
                TotalAmountInQueue = vipCustomers.Sum(x => x.Amount) + basicCustomers.Sum(x => x.Amount),
                TotalAmountCashOut = historyCustomers.Sum(x => x.Amount),
                CustomerCashOutSuccess = historyCustomers
            };

            historyCustomers.ToList().ForEach(customer => Console.WriteLine(
              $"\n{index += 1}. {customer.Name}"));

            Console.WriteLine($"\nTổng số tiền còn lại trong hàng đợi: " + string.Format(new CultureInfo("vi-VN"), "{0:C}", reportBank.TotalAmountInQueue) +
              $"\nTổng số tiền đã rút: " + string.Format(new CultureInfo("vi-VN"), "{0:C}", reportBank.TotalAmountCashOut));

            var listCount = historyCustomers.Count == 0 ? throw new Exception("List is empty!") : historyCustomers.Count;
            return reportBank;
        }
    }
}