﻿using DeveloperBank.Interfaces;
using DeveloperBank.Services;
using DeveloperBank.View;

ILogic logic = new Logic();
IView view = new View(logic);
view.HandleMenu();