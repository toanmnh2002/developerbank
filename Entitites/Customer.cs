﻿namespace DeveloperBank.Entitites
{
    public class Customer
    {
        public string CustomerId { get; set; }
        public string Name { get; set; } = null!;
        public decimal Amount { get; set; }
    }
}