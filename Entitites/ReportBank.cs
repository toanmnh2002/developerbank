﻿using DeveloperBank.Entitites;

namespace DeveloperBank.Entities
{
    public class ReportBank
    {
        public List<Customer>? CustomerCashOutSuccess { get; set; }
        public decimal TotalAmountCashOut { get; set; }
        public decimal TotalAmountInQueue { get; set; }
    }
}