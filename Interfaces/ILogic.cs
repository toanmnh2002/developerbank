﻿using DeveloperBank.Entities;

namespace DeveloperBank.Interfaces
{
    public interface ILogic
    {
        void AddCustomerToTheQueue();
        void CallNextBasicCustomer();
        void CallNextVipCustomer();
        void CallUpcomingCustomer();
        ReportBank ReportSystem();
    }
}