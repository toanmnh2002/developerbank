﻿namespace DeveloperBank.Interfaces
{
    public interface IView
    {
        void HandleMenu();
    }
}