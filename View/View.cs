﻿using DeveloperBank.Interfaces;

namespace DeveloperBank.View
{
    public class View : IView
    {
        private readonly ILogic _logic;

        public View(ILogic logic)
        {
            _logic = logic;
        }

        public void HandleMenu()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            bool back = false;
            do
            {
                try
                {

                    Menu();
                    Console.Write("Nhập số tương ứng: ");
                    int.TryParse(Console.ReadLine(), out int choice);

                    switch (choice)
                    {
                        case 1:
                            _logic.AddCustomerToTheQueue();
                            break;
                        case 2:
                            _logic.CallNextBasicCustomer();
                            break;
                        case 3:
                            _logic.CallNextVipCustomer();
                            break;
                        case 4:
                            _logic.CallUpcomingCustomer();
                            break;
                        case 5:
                            _logic.ReportSystem();
                            break;
                        case 6:
                            back = !ConfirmYesNo("Bạn muốn tiếp tục chứ(y/n)?: ");
                            break;
                        default:
                            throw new Exception("Lựa chọn không hợp lệ");
                    }

                }

                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                    Console.ResetColor();
                }

            } while (!back);
        }

        private void Menu()
        {
            Console.WriteLine("----------- Quản lý Hàng đợi Ngân hàng -----------");
            Console.WriteLine("1. Thêm khách hàng vào hàng đợi");
            Console.WriteLine("2. Gọi tên khách hàng VIP tiếp theo");
            Console.WriteLine("3. Gọi tên khách hàng Thường tiếp theo");
            Console.WriteLine("4. Hiển thị khách hàng sắp tới");
            Console.WriteLine("5. Thống kê");
            Console.WriteLine("6. Thoát chương trình");
        }

        private static bool ConfirmYesNo(string message)
        {
            Console.Write(message);
            string answer = Console.ReadLine();
            var check = answer.Equals("yes", StringComparison.OrdinalIgnoreCase) || answer.Equals("y", StringComparison.OrdinalIgnoreCase);
            return check;
        }
    }
}